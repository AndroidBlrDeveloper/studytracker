package by.shapovalov.studytracker.ui.activity;

import android.os.Bundle;

import by.shapovalov.studytracker.R;
import by.shapovalov.studytracker.ui.fragment.SignInFragment;
import by.shapovalov.studytracker.util.manager.FragmentTransactionManager;

public class AuthorizingActivity extends BaseActivity {

    // ===========================================================
    // Constants
    // ===========================================================

    private String LOG_TAG = AuthorizingActivity.class.getSimpleName();

    // ===========================================================
    // Fields
    // ===========================================================

    // ===========================================================
    // Constructors
    // ===========================================================

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass
    // ===========================================================

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            FragmentTransactionManager.displayFragment(
                    getSupportFragmentManager(),
                    SignInFragment.newInstance(),
                    R.id.fl_authorizing_container,
                    false
            );
        }
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_authorizing;
    }

    // ===========================================================
    // Listeners, methods for/from Interfaces
    // ===========================================================

    // ===========================================================
    // Methods
    // ===========================================================

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

}