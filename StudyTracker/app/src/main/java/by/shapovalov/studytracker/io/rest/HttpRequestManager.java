package by.shapovalov.studytracker.io.rest;

import android.content.Context;
import android.os.Bundle;

import by.shapovalov.studytracker.io.rest.entity.HttpConnection;
import by.shapovalov.studytracker.io.rest.util.HttpResponseUtil;

public class HttpRequestManager {

    // ===========================================================
    // Constants
    // ===========================================================

    private static final String LOG_TAG = HttpRequestManager.class.getSimpleName();

    // ===========================================================
    // Methods
    // ===========================================================

    /**
     * @param url api url to connect
     * @param token pass authorization token if required, otherwise pass null
     * @param value pass any additional data (usually post request json entity) if required, otherwise pass null
     * @return executed result of API request
     */

    // ////////////////////////////////////////////////////////////
    // execute request and return result
    public static HttpConnection executeRequest(Context context, String requestMethod, String url,
                                                String token, String value) {

        /* Attach all additional data like token, header values, json values for post request and so on (if necessary) */
        Bundle bundle = new Bundle();
        /* Value - for POST requests (if we need to send some json value on server) */
        bundle.putString(RestHttpClient.BundleUtil.POST_REQUEST_JSON_ENTITY, value);
        /* Token - authorization token */
        bundle.putString(RestHttpClient.BundleUtil.TOKEN, token);

        /* Call API for data */
        HttpConnection httpConnection = null;
        if (requestMethod.equals(RestHttpClient.RequestMethod.POST)) {
            httpConnection = RestHttpClient.executePostRequest(context, url, bundle);
        } else if (requestMethod.equals(RestHttpClient.RequestMethod.GET)) {
            httpConnection = RestHttpClient.executeGetRequest(context, url, bundle);
        }

        /* Handle connection */
        httpConnection = HttpResponseUtil.handleConnection(httpConnection);

        return httpConnection;
    }

}
