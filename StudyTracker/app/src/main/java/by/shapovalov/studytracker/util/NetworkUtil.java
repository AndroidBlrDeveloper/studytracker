package by.shapovalov.studytracker.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkUtil {


    // ===========================================================
    // Constants
    // ===========================================================

    private static final String LOG_TAG = NetworkUtil.class.getSimpleName();

    // ===========================================================
    // Fields
    // ===========================================================

    private Context mContext;
    private static NetworkUtil sInstance;

    // ===========================================================
    // Constructors
    // ===========================================================

    private NetworkUtil(Context mContext) {
        this.mContext = mContext;
    }

    public static NetworkUtil getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new NetworkUtil(context);
        }
        return sInstance;
    }

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass
    // ===========================================================

    // ===========================================================
    // Listeners, methods for/from Interfaces
    // ===========================================================

    // ===========================================================
    // Methods
    // ===========================================================

    public boolean isConnected() throws NullPointerException {
        ConnectivityManager connectivityManager = (ConnectivityManager)
                mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnectedOrConnecting();
    }

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

}
