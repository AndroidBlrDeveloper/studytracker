package by.shapovalov.studytracker.io.rest.service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;

import by.shapovalov.studytracker.io.rest.HttpRequestManager;
import by.shapovalov.studytracker.io.rest.RestHttpClient;
import by.shapovalov.studytracker.io.rest.entity.HttpConnection;
import by.shapovalov.studytracker.io.rest.util.HttpErrorUtil;
import by.shapovalov.studytracker.util.Constant;
import by.shapovalov.studytracker.util.Preference;


public class STIntentService extends IntentService {

    // ===========================================================
    // Constants
    // ===========================================================

    private static final String LOG_TAG = STIntentService.class.getSimpleName();

    public static final int TEST_USER_ID = 1;
    public static final String TEST_GCM_REG_ID = "2453577675879";
    public static final String TEST_DEVICE_ID = "235246112";
    public static final String TEST_NAME = "name";

    private class Extra {
        public static final String VALUE_ADDITIONAL = "VALUE_ADDITIONAL";
        public static final String VALUE_URL = "VALUE_URL";
        public static final String VALUE_MESSENGER = "VALUE_MESSENGER";
        public static final String VALUE_REQUEST_TYPE = "VALUE_REQUEST_TYPE";
        public static final String VALUE_REQUEST_MODE = "VALUE_REQUEST_MODE";
    }

    // ===========================================================
    // Fields
    // ===========================================================

    // ===========================================================
    // Constructors
    // ===========================================================

    public STIntentService() {
        super(STIntentService.class.getName());
    }

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Listeners, methods for/from Interfaces
    // ===========================================================

    // ===========================================================
    // Helper methods
    // ===========================================================

    /**
     * @param callback    - handler callback that helps us to send and receive results in UI
     * @param url         - calling api url
     * @param requestType - string constant that helps us to distinguish what request it is
     * @param value       - usually "value" is a POST request entity (json string that must be sent on server) or something else
     * @param requestMode - is used to distinguish by pagination is it init, next or update load
     */

    public static void start(Context context, Handler.Callback callback, String url, int requestType, String value, int requestMode) {
        Intent intent = new Intent(Intent.ACTION_SYNC, null, context, STIntentService.class);
        intent.putExtra(Extra.VALUE_URL, url);
        intent.putExtra(Extra.VALUE_REQUEST_TYPE, requestType);
        intent.putExtra(Extra.VALUE_MESSENGER, new Messenger(new Handler(callback)));
        intent.putExtra(Extra.VALUE_ADDITIONAL, value);
        intent.putExtra(Extra.VALUE_REQUEST_MODE, requestMode);
        context.startService(intent);
    }

    public static void start(Context context, Handler.Callback callback, String url, int requestType, int requestMode) {
        Intent intent = new Intent(Intent.ACTION_SYNC, null, context, STIntentService.class);
        intent.putExtra(Extra.VALUE_URL, url);
        intent.putExtra(Extra.VALUE_REQUEST_TYPE, requestType);
        intent.putExtra(Extra.VALUE_MESSENGER, new Messenger(new Handler(callback)));
        intent.putExtra(Extra.VALUE_REQUEST_MODE, requestMode);
        context.startService(intent);
    }

    public static void start(Context context, Handler.Callback callback, String url, int requestType, String value) {
        Intent intent = new Intent(Intent.ACTION_SYNC, null, context, STIntentService.class);
        intent.putExtra(Extra.VALUE_URL, url);
        intent.putExtra(Extra.VALUE_REQUEST_TYPE, requestType);
        intent.putExtra(Extra.VALUE_MESSENGER, new Messenger(new Handler(callback)));
        intent.putExtra(Extra.VALUE_ADDITIONAL, value);
        context.startService(intent);
    }

    public static void start(Context context, Handler.Callback callback, String url, int requestType) {
        Intent intent = new Intent(Intent.ACTION_SYNC, null, context, STIntentService.class);
        intent.putExtra(Extra.VALUE_URL, url);
        intent.putExtra(Extra.VALUE_REQUEST_TYPE, requestType);
        intent.putExtra(Extra.VALUE_MESSENGER, new Messenger(new Handler(callback)));
        context.startService(intent);
    }

    public static void stop(Context context) {
        context.stopService(new Intent(context, STIntentService.class));
    }

    private void sendMessage(Messenger messenger, int message, Bundle bundle) {
        if (messenger != null) {
            Message msg = Message.obtain();
            msg.what = message;

            // set any additional data
            if (bundle != null) {
                msg.setData(bundle);
            }

            try {
                // send msg to UI
                messenger.send(msg);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    // ===========================================================
    // Methods for/from SuperClass
    // ===========================================================

    @Override
    protected void onHandleIntent(Intent intent) {
        // setIntentRedeliveryBehavior(START_STICKY);

        // get data from intent
        int requestType = intent.getExtras().getInt(Extra.VALUE_REQUEST_TYPE);
        int requestMode = intent.getExtras().getInt(Extra.VALUE_REQUEST_MODE);
        String url = intent.getExtras().getString(Extra.VALUE_URL);
        String value = intent.getExtras().getString(Extra.VALUE_ADDITIONAL);
        Messenger messenger = (Messenger) intent.getExtras().get(Extra.VALUE_MESSENGER);
        Log.i(LOG_TAG, requestType + Constant.Symbol.SPACE + url);

        // detect type of request we have to execute
        switch (requestType) {

            case Constant.RequestType.SIGN_IN:
                signInRequest(url, value, messenger);
                break;

            case Constant.RequestType.SIGN_UP:
                signUpRequest(url, value, messenger);
                break;

        }
    }

    // ===========================================================
    // Methods
    // ===========================================================

    ///////////////////////////////////////////////////////////////
    // sign in
    private void signInRequest(String url, String value, Messenger messenger) {
        HttpConnection httpConnection = HttpRequestManager.executeRequest(
                this,
                RestHttpClient.RequestMethod.POST,
                url,
                null,
                value
        );

        Bundle bundle = new Bundle();

        if (httpConnection.isHttpConnectionSucceeded()) {

            Preference.getInstance(this).setUserToken(httpConnection.getResponseHeader().getToken());

            // parse response and attache bundle to pass in UI
            // inform UI about sign up complete
            sendMessage(messenger, Constant.Message.Success.SIGN_IN_COMPLETED, null);

        } else {
            Log.e(LOG_TAG, httpConnection.getHttpConnectionMessage()
                    + Constant.Symbol.SPACE + httpConnection.getHttpConnectionCode());

            if (httpConnection.getHttpConnectionCode() == HttpErrorUtil.NumericStatusCode.HTTP_NO_NETWORK) {
                sendMessage(messenger, Constant.Message.Error.NO_NETWORK, null);

            } else if (httpConnection.getHttpConnectionCode() == HttpErrorUtil.NumericStatusCode.HTTP_UNAUTHORIZED) {
                /*sendMessage(messenger, Constant.Message.Error.UNAUTHORIZED, null);*/

            } else if (httpConnection.getHttpConnectionCode() == HttpErrorUtil.NumericStatusCode.HTTP_BAD_REQUEST) {
                /*// TODO: Parse error json
                bundle.putString(Constant.Message.Extra.VALUE_BAD_REQUEST, httpConnection.getHttpResponseBody().toString());
                sendMessage(messenger, Constant.Message.Error.BAD_REQUEST, bundle);*/

            } else if (httpConnection.getHttpConnectionCode() == HttpErrorUtil.NumericStatusCode.HTTP_NOT_FOUND) {
                sendMessage(messenger, Constant.Message.Error.PAGE_NOT_FOUND, null);

            } else {
                sendMessage(messenger, Constant.Message.Error.UNHANDLED, null);
            }
        }
    }

    ///////////////////////////////////////////////////////////////
    // sign up
    private void signUpRequest(String url, String value, Messenger messenger) {
        HttpConnection httpConnection = HttpRequestManager.executeRequest(
                this,
                RestHttpClient.RequestMethod.POST,
                url,
                null,
                value
        );

        Bundle bundle = new Bundle();

        if (httpConnection.isHttpConnectionSucceeded()) {

            // parse response and attache bundle to pass in UI
            // inform UI about sign up complete
            sendMessage(messenger, Constant.Message.Success.SIGN_UP_COMPLETED, null);

        } else {
            Log.e(LOG_TAG, httpConnection.getHttpConnectionMessage()
                    + Constant.Symbol.SPACE + httpConnection.getHttpConnectionCode());

            if (httpConnection.getHttpConnectionCode() == HttpErrorUtil.NumericStatusCode.HTTP_NO_NETWORK) {
                sendMessage(messenger, Constant.Message.Error.NO_NETWORK, null);

            } else if (httpConnection.getHttpConnectionCode() == HttpErrorUtil.NumericStatusCode.HTTP_UNAUTHORIZED) {
                /*sendMessage(messenger, Constant.Message.Error.UNAUTHORIZED, null);*/

            } else if (httpConnection.getHttpConnectionCode() == HttpErrorUtil.NumericStatusCode.HTTP_BAD_REQUEST) {
                /*// TODO: Parse error json
                bundle.putString(Constant.Message.Extra.VALUE_BAD_REQUEST, httpConnection.getHttpResponseBody().toString());
                sendMessage(messenger, Constant.Message.Error.BAD_REQUEST, bundle);*/

            } else if (httpConnection.getHttpConnectionCode() == HttpErrorUtil.NumericStatusCode.HTTP_NOT_FOUND) {
                sendMessage(messenger, Constant.Message.Error.PAGE_NOT_FOUND, null);

            } else {
                sendMessage(messenger, Constant.Message.Error.UNHANDLED, null);
            }
        }
    }

}