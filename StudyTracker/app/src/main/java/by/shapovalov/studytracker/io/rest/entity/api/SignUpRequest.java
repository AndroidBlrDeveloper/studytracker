package by.shapovalov.studytracker.io.rest.entity.api;

import com.google.gson.annotations.SerializedName;

public class SignUpRequest {

    @SerializedName("email")
    private String email;

    /*@SerializedName("username")
    private String username;*/

    @SerializedName("password")
    private String password;

    public SignUpRequest() {
    }

    public SignUpRequest(String email, String password) {
        this.email = email;
        this.password = password;
    }

    /*public SignUpRequest(String email, String username, String password) {
        this.email = email;
        this.username = username;
        this.password = password;
    }*/

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    /*public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }*/
}
