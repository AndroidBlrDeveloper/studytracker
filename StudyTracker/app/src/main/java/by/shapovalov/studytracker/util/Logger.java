package by.shapovalov.studytracker.util;

public class Logger {

    public static final String UNHANDLED_ERROR = "Unhandled error ";
    public static final String INTERNET_CONNECTION_ERROR = "Internet connection error ";
    public static final String USER_NOT_AUTHORIZED_ERROR = "User not authorized ";
    public static final String SERVER_CONNECTION_ERROR = "Server connection error ";
    public static final String BAD_REQUEST_ERROR = "Bad request ";
    public static final String CONNECTION_TIMEOUT_ERROR = "Connection time out ";
    public static final String PAGE_NOT_FOUND_ERROR = "Page not found ";
    public static final String INTERNET_CONNECTION_RESUMED = "Internet connection resumed ";
    public static final String URL = "Calling URL: ";
    public static final String TOKEN = "Token: ";
    public static final String ETAG = "ETag: ";
    public static final String SERVER_RESPONSE = "Server response: ";
    public static final String LAST_MODIFIED = "Last-Modified: ";
}
