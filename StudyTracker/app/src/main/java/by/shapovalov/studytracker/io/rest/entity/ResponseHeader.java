package by.shapovalov.studytracker.io.rest.entity;

public class ResponseHeader {

    private String token;
    private String eTag;
    private String lastModified;

    public ResponseHeader(String token, String eTag) {
        this.token = token;
        this.eTag = eTag;
    }

    public ResponseHeader(String token, String eTag, String lastModified) {
        this.token = token;
        this.eTag = eTag;
        this.lastModified = lastModified;
    }

    public ResponseHeader() {
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getETag() {
        return eTag;
    }

    public void setETag(String eTag) {
        this.eTag = eTag;
    }

    public String getLastModified() {
        return lastModified;
    }

    public void setLastModified(String lastModified) {
        this.lastModified = lastModified;
    }

}
