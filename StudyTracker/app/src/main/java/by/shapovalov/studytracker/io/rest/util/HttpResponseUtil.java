package by.shapovalov.studytracker.io.rest.util;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;

import by.shapovalov.studytracker.io.rest.entity.HttpConnection;
import by.shapovalov.studytracker.io.rest.entity.ResponseHeader;
import by.shapovalov.studytracker.util.Constant;
import by.shapovalov.studytracker.util.Logger;

public class HttpResponseUtil {


    // ===========================================================
    // Constants
    // ===========================================================

    private static final String LOG_TAG = HttpResponseUtil.class.getSimpleName();

    // ===========================================================
    // Fields
    // ===========================================================

    // ===========================================================
    // Constructors
    // ===========================================================

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass
    // ===========================================================

    // ===========================================================
    // Listeners, methods for/from Interfaces
    // ===========================================================

    // ===========================================================
    // Methods
    // ===========================================================

    public static HttpConnection handleConnection(HttpConnection httpConnection) {

        StringBuilder responseStrBuilder = null;
        HttpURLConnection httpURLConnection = httpConnection.getHttpURLConnection();

        if (httpURLConnection != null) {

            InputStream httpInputStream;
            InputStreamReader inputStreamReader = null;
            String httpResponseMsg;
            int httpResponseCode;

            try {

                httpResponseCode = httpURLConnection.getResponseCode();
                httpResponseMsg = httpURLConnection.getResponseMessage();
                Log.i(LOG_TAG, String.valueOf(httpResponseCode) + Constant.Symbol.SPACE + httpResponseMsg);

                // 2XX: generally "OK", 3XX: relocation/redirect, 4XX: client error, 5XX: server error
                if (httpResponseCode <= HttpURLConnection.HTTP_BAD_REQUEST) {

                    // 400 bad request (e.g "Email already exist", "Invalid user name" and so on)
                    if (httpResponseCode == HttpURLConnection.HTTP_BAD_REQUEST) {
                        httpInputStream = httpURLConnection.getErrorStream();
                        httpConnection.setHttpConnectionSucceeded(false);

                    } else {
                        httpInputStream = httpURLConnection.getInputStream();
                        httpConnection.setHttpConnectionSucceeded(true);
                    }

                    inputStreamReader = new InputStreamReader(httpInputStream);
                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                    String tempLine = bufferedReader.readLine();
                    responseStrBuilder = new StringBuilder();
                    while (tempLine != null) {
                        responseStrBuilder.append(tempLine);
                        tempLine = bufferedReader.readLine();
                    }
                    bufferedReader.close();
                    Log.i(LOG_TAG, responseStrBuilder.toString());

                    httpConnection.setHttpConnectionCode(httpResponseCode);
                    httpConnection.setHttpConnectionMessage(httpResponseMsg);
                    httpConnection.setResponseHeader(readHeader(httpConnection));
                    httpConnection.setHttpResponseBody(responseStrBuilder);

                } else {
                    httpConnection.setHttpConnectionSucceeded(false);
                    httpConnection.setHttpConnectionCode(httpResponseCode);
                    httpConnection.setHttpConnectionMessage(httpResponseMsg);
                    httpConnection.setResponseHeader(readHeader(httpConnection));
                    httpConnection.setHttpResponseBody(null);


                    if (httpResponseCode == HttpErrorUtil.NumericStatusCode.HTTP_UNAUTHORIZED) {
                        // 401 user not authorized
                        Log.e(LOG_TAG, Logger.USER_NOT_AUTHORIZED_ERROR);

                    } else if (httpResponseCode == HttpErrorUtil.NumericStatusCode.HTTP_NOT_FOUND) {
                        //404 File/Page not found
                        Log.e(LOG_TAG, Logger.PAGE_NOT_FOUND_ERROR);

                    } else {
                        // Other errors while connecting server (405, 500, 501 or else)
                        Log.e(LOG_TAG, Logger.SERVER_CONNECTION_ERROR);
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();

            } finally {
                try {
                    httpURLConnection.disconnect();
                    if (inputStreamReader != null) {
                        inputStreamReader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return httpConnection;
    }

    public static ResponseHeader readHeader(HttpConnection httpConnection) {

        //NOTE: find correct place
        class Header {
            private static final String AUTHORIZATION = "Authorization";
            private static final String CONTENT_TYPE = "Content-Type";
            private static final String X_TOKEN = "X-Token";
            private static final String E_TAG = "eTag";
            private static final String LAST_MODIFIED = "Last-Modified";
        }

        ResponseHeader responseHeader = new ResponseHeader();

        try {

            int httpResponseCode = httpConnection.getHttpConnectionCode();
            String httpResponseMsg = httpConnection.getHttpConnectionMessage();
            Log.i(LOG_TAG, String.valueOf(httpResponseCode) + Constant.Symbol.SPACE + httpResponseMsg);

            if (httpConnection.getHttpURLConnection() != null) {

                responseHeader.setToken(httpConnection.getHttpURLConnection().getHeaderField(Header.X_TOKEN));
                responseHeader.setETag(httpConnection.getHttpURLConnection().getHeaderField(Header.E_TAG));
                responseHeader.setLastModified(httpConnection.getHttpURLConnection().getHeaderField(Header.LAST_MODIFIED));

                Log.i(LOG_TAG, Logger.TOKEN + responseHeader.getToken());
                Log.i(LOG_TAG, Logger.ETAG + responseHeader.getETag());
                Log.i(LOG_TAG, Logger.LAST_MODIFIED + responseHeader.getLastModified());

            } else {
                responseHeader.setToken(null);
                responseHeader.setETag(null);
                responseHeader.setLastModified(null);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return responseHeader;
    }

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

}
