package by.shapovalov.studytracker.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;

import by.shapovalov.studytracker.R;
import by.shapovalov.studytracker.io.rest.entity.api.SignInRequest;
import by.shapovalov.studytracker.io.rest.service.STIntentService;
import by.shapovalov.studytracker.ui.activity.MainActivity;
import by.shapovalov.studytracker.util.Constant;
import by.shapovalov.studytracker.util.EdittextValidateResponse;
import by.shapovalov.studytracker.util.EdittextValidator;
import by.shapovalov.studytracker.util.manager.FragmentTransactionManager;

public class SignInFragment extends BaseFragment implements View.OnClickListener, Handler.Callback {

    // ===========================================================
    // Constants
    // ===========================================================

    private String LOG_TAG = SignInFragment.class.getSimpleName();

    // ===========================================================
    // Fields
    // ===========================================================

    private TextView mTvSignUp;
    private TextInputEditText mTieUsername;
    private TextInputEditText mTiePassword;
    private Button mBtnSignIn;

    // ===========================================================
    // Constructors
    // ===========================================================

    public static SignInFragment newInstance() {
        return new SignInFragment();
    }

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass
    // ===========================================================

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sign_in, container, false);
        findViews(view);
        setListeners();

        return view;
    }


    // ===========================================================
    // Listeners, methods for/from Interfaces
    // ===========================================================

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btn_sign_in:
                grabDataAndProceed(v);
                break;

            case R.id.tv_sign_in_register:
                FragmentTransactionManager.displayFragment(
                        getFragmentManager(),
                        SignUpFragment.newInstance(),
                        R.id.fl_authorizing_container,
                        true
                );

                break;
        }
    }

    @Override
    public boolean handleMessage(Message msg) {
        switch (msg.what) {

            case Constant.Message.Success.SIGN_IN_COMPLETED:
                Snackbar.make(getActivity().getWindow().getDecorView(), "Logined", Snackbar.LENGTH_LONG);

                Intent intent = new Intent(getActivity(), MainActivity.class);
                startActivity(intent);

                break;

            //TODO: handle errors

        }

        return false;
    }

    // ===========================================================
    // Methods
    // ===========================================================

    private void setListeners() {
        mBtnSignIn.setOnClickListener(this);
        mTvSignUp.setOnClickListener(this);
    }

    private void findViews(View view) {
        mBtnSignIn = (Button) view.findViewById(R.id.btn_sign_in);
        mTieUsername = (TextInputEditText) view.findViewById(R.id.tie_sign_in_username);
        mTiePassword = (TextInputEditText) view.findViewById(R.id.tie_sign_in_password);
        mTvSignUp = (TextView) view.findViewById(R.id.tv_sign_in_register);
    }

    private void grabDataAndProceed(View clickedView) {
        EdittextValidateResponse edittextValidateResponse = EdittextValidator.checkIsEmpty(
                mTieUsername,
                mTiePassword
        );

        if (!edittextValidateResponse.isResponseSuccess()) {
            EdittextValidator.handleEmptyState(
                    edittextValidateResponse,
                    getString(R.string.edt_error)
            );

        } else {
            SignInRequest signInRequest = new SignInRequest();
            signInRequest.setUsername(mTieUsername.getText().toString());
            signInRequest.setPassword(mTiePassword.getText().toString());

            STIntentService.start(
                    getActivity(),
                    this,
                    Constant.ApiURL.SIGN_IN,
                    Constant.RequestType.SIGN_IN,
                    new Gson().toJson(signInRequest)
            );
        }
    }

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

}