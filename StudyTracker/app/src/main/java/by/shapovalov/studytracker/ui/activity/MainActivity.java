package by.shapovalov.studytracker.ui.activity;

import android.os.Bundle;

import by.shapovalov.studytracker.R;
import by.shapovalov.studytracker.ui.fragment.ChatFragment;
import by.shapovalov.studytracker.util.manager.FragmentTransactionManager;


public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            FragmentTransactionManager.displayFragment(
                    getSupportFragmentManager(),
                    new ChatFragment(),
                    R.id.fl_main_container,
                    false
            );
        }
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_main;
    }
}
