package by.shapovalov.studytracker.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Preference {

    // ===========================================================
    // Constants
    // ===========================================================

    private static final String USER_TOKEN = "USER_TOKEN";

    // ===========================================================
    // Fields
    // ===========================================================

    private static Preference sInstance;
    private SharedPreferences mSharedPreferences;
    private SharedPreferences.Editor mEditor;
    private Context mContext;

    // ===========================================================
    // Constructors
    // ===========================================================

    private Preference(Context context) {
        mContext = context;
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        mEditor = mSharedPreferences.edit();
    }

    public static Preference getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new Preference(context);
        }

        return sInstance;
    }

    // ===========================================================
    // Getter & Setter
    // ===========================================================


    public void setUserToken(String token) {
        mEditor.putString(USER_TOKEN, token);
        commit();
    }

    public String getUserToken() {
        return mSharedPreferences.getString(USER_TOKEN, null);
    }

    // ===========================================================
    // Listeners
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    // ===========================================================
    // Methods
    // ===========================================================

    public void commit() {
        mEditor.apply();
    }

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

}
