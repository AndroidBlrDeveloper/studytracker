package by.shapovalov.studytracker.io.rest.entity;

import java.net.HttpURLConnection;

public class HttpConnection {

    private int httpConnectionCode;
    private boolean httpConnectionSucceeded;
    private String httpConnectionMessage;
    private HttpURLConnection httpURLConnection;
    private StringBuilder httpResponseBody;
    private ResponseHeader responseHeader;

    public HttpConnection(int httpConnectionCode, String httpConnectionMessage,
                          HttpURLConnection httpURLConnection) {
        this.httpConnectionCode = httpConnectionCode;
        this.httpConnectionMessage = httpConnectionMessage;
        this.httpURLConnection = httpURLConnection;
    }

    public HttpConnection(int httpConnectionCode, boolean httpConnectionSucceeded,
                          String httpConnectionMessage, HttpURLConnection httpURLConnection) {
        this.httpConnectionCode = httpConnectionCode;
        this.httpConnectionSucceeded = httpConnectionSucceeded;
        this.httpConnectionMessage = httpConnectionMessage;
        this.httpURLConnection = httpURLConnection;
    }

    public HttpConnection(boolean httpConnectionSucceeded, String httpConnectionMessage, int httpConnectionCode,
                          HttpURLConnection httpURLConnection, StringBuilder httpResponseBody) {
        this.httpConnectionSucceeded = httpConnectionSucceeded;
        this.httpConnectionMessage = httpConnectionMessage;
        this.httpConnectionCode = httpConnectionCode;
        this.httpURLConnection = httpURLConnection;
        this.httpResponseBody = httpResponseBody;
    }

    public HttpConnection(int httpConnectionCode, boolean httpConnectionSucceeded, String httpConnectionMessage,
                          HttpURLConnection httpURLConnection, StringBuilder httpResponseBody, ResponseHeader responseHeader) {
        this.httpConnectionCode = httpConnectionCode;
        this.httpConnectionSucceeded = httpConnectionSucceeded;
        this.httpConnectionMessage = httpConnectionMessage;
        this.httpURLConnection = httpURLConnection;
        this.httpResponseBody = httpResponseBody;
        this.responseHeader = responseHeader;
    }

    public HttpConnection() {
    }

    public HttpURLConnection getHttpURLConnection() {
        return httpURLConnection;
    }

    public void setHttpURLConnection(HttpURLConnection httpURLConnection) {
        this.httpURLConnection = httpURLConnection;
    }

    public int getHttpConnectionCode() {
        return httpConnectionCode;
    }

    public void setHttpConnectionCode(int httpConnectionErrorCode) {
        this.httpConnectionCode = httpConnectionErrorCode;
    }

    public String getHttpConnectionMessage() {
        return httpConnectionMessage;
    }

    public void setHttpConnectionMessage(String httpConnectionErrorMessage) {
        this.httpConnectionMessage = httpConnectionErrorMessage;
    }

    public boolean isHttpConnectionSucceeded() {
        return httpConnectionSucceeded;
    }

    public void setHttpConnectionSucceeded(boolean httpConnectionEstablished) {
        this.httpConnectionSucceeded = httpConnectionEstablished;
    }

    public StringBuilder getHttpResponseBody() {
        return httpResponseBody;
    }

    public void setHttpResponseBody(StringBuilder httpRequestResponseBody) {
        this.httpResponseBody = httpRequestResponseBody;
    }

    public ResponseHeader getResponseHeader() {
        return responseHeader;
    }

    public void setResponseHeader(ResponseHeader responseHeader) {
        this.responseHeader = responseHeader;
    }
}
