package by.shapovalov.studytracker.util;

import android.widget.EditText;

public class EdittextValidator {

    public static EdittextValidateResponse checkIsEmpty(EditText... editTexts) {
        EdittextValidateResponse edittextValidateResponse = new EdittextValidateResponse();

        for (EditText editText : editTexts) {
            if (editText.getText().toString().isEmpty()) {
                edittextValidateResponse.addErrorEdt(editText);
            }
        }

        return edittextValidateResponse;
    }

    public static void handleEmptyState(EdittextValidateResponse edittextValidateResponse, String error) {
        for (int i = 0; i < edittextValidateResponse.getEmptyEdtList().size(); ++i) {

            if (i == 0) {
                edittextValidateResponse.getEmptyEdtList().get(i).requestFocus();
            }
            edittextValidateResponse.getEmptyEdtList().get(i).setError(error);
        }
    }

}
