package by.shapovalov.studytracker.io.rest;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import by.shapovalov.studytracker.io.rest.entity.HttpConnection;
import by.shapovalov.studytracker.io.rest.util.HttpErrorUtil;
import by.shapovalov.studytracker.util.Constant;
import by.shapovalov.studytracker.util.Logger;
import by.shapovalov.studytracker.util.NetworkUtil;

public class RestHttpClient {

    // ===========================================================
    // Constants
    // ===========================================================

    private static final String LOG_TAG = RestHttpClient.class.getSimpleName();

    private static final String UTF_8 = "UTF-8";
    private static final String TOKEN_VALUE = "Token ";

    public class Header {
        private static final String AUTHORIZATION = "Authorization";
        private static final String CONTENT_TYPE = "Content-Type";
        private static final String ACCEPT_ENCODING = "Accept-Encoding";
    }

    public class PayloadType {
        private static final String APPLICATION_JSON = "application/json";
    }

    public class RequestMethod {
        public static final String POST = "POST";
        public static final String GET = "GET";
        public static final String PUT = "PUT";
        public static final String HEAD = "HEAD";
        public static final String DELETE = "DELETE";
    }

    /**
     * All extra data like
     * headers,
     * tokens,
     * json post values
     * will be put to bundle
     */
    public class BundleUtil {
        public static final String TOKEN = "token";
        public static final String POST_REQUEST_JSON_ENTITY = "entity";
    }

    // ===========================================================
    // Fields
    // ===========================================================

    // ===========================================================
    // Constructors
    // ===========================================================

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods
    // ===========================================================

    /**
     * HEAD Request
     */
    public static HttpConnection executeHeadRequest(Context context, String url, Bundle bundle) {
        return null;
    }

    /**
     * POST Request
     */
    public static HttpConnection executePostRequest(Context context, String url, Bundle bundle) {
        Object token = null;
        String value = null;
        if (bundle != null) {
            token = bundle.get(BundleUtil.TOKEN);
            value = String.valueOf(bundle.get(BundleUtil.POST_REQUEST_JSON_ENTITY));
        }

        HttpConnection httpConnection = new HttpConnection();
        HttpURLConnection httpURLConnection = null;

        try {
            Log.i(LOG_TAG, Logger.URL + url);
            httpURLConnection = (HttpURLConnection) new URL(url).openConnection();

            if (token != null) {
                httpURLConnection.setRequestProperty(Header.AUTHORIZATION, TOKEN_VALUE + token);
                Log.i(LOG_TAG, Logger.TOKEN + String.valueOf(token));
            }

            httpURLConnection.setRequestMethod(RequestMethod.POST);
            httpURLConnection.setRequestProperty(Header.CONTENT_TYPE, PayloadType.APPLICATION_JSON);
            httpURLConnection.setRequestProperty(Header.ACCEPT_ENCODING, Constant.Symbol.NULL);
            httpURLConnection.setUseCaches(false);
            httpURLConnection.setDoInput(true);
            httpURLConnection.setDoOutput(true);
            httpURLConnection.connect();

            OutputStream out = httpURLConnection.getOutputStream();
            out.write(value.getBytes(UTF_8));
            out.flush();
            out.close();

            Log.i(LOG_TAG, Logger.SERVER_RESPONSE + String.valueOf(httpURLConnection.getResponseCode())
                    + Constant.Symbol.SPACE + httpURLConnection.getResponseMessage());

            httpConnection.setHttpURLConnection(httpURLConnection);
            httpConnection.setHttpConnectionSucceeded(true);
            httpConnection.setHttpConnectionCode(httpURLConnection.getResponseCode());
            httpConnection.setHttpConnectionMessage(httpURLConnection.getResponseMessage());

        } catch (Exception e) {
            e.printStackTrace();

            // Connection not established, disconnect httpURLConnection
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }

            if (!NetworkUtil.getInstance(context).isConnected()) {
                // internet connection error
                Log.e(LOG_TAG, Logger.INTERNET_CONNECTION_ERROR);

                httpConnection.setHttpConnectionSucceeded(false);
                httpConnection.setHttpConnectionCode(HttpErrorUtil.NumericStatusCode.HTTP_NO_NETWORK);
                httpConnection.setHttpConnectionMessage(e.getMessage());

            } else if (e.getMessage().contains(HttpErrorUtil.NonNumericStatusCode.HTTP_SERVER_TIMEOUT)) {
                // Connection timed out occurred while calling server
                Log.e(LOG_TAG, Logger.CONNECTION_TIMEOUT_ERROR + url);

                httpConnection.setHttpConnectionSucceeded(false);
                httpConnection.setHttpConnectionCode(HttpErrorUtil.NumericStatusCode.HTTP_SERVER_TIMEOUT);
                httpConnection.setHttpConnectionMessage(e.getMessage());

            } else {
                // Other connection errors e.g UnknownHostException: No address associated with hostname
                Log.e(LOG_TAG, e.getMessage());

                httpConnection.setHttpConnectionSucceeded(false);
                httpConnection.setHttpConnectionCode(HttpErrorUtil.NumericStatusCode.HTTP_UNKNOWN_ERROR);
                httpConnection.setHttpConnectionMessage(e.getMessage());
            }
        }

        return httpConnection;
    }

    /**
     * PUT Request
     */
    public static HttpConnection executePutRequest(Context context, String url, Bundle bundle) {
        return null;
    }

    /**
     * GET Request
     */
    public static HttpConnection executeGetRequest(final Context context, String url, Bundle bundle) {
        Object token = null;
        if (bundle != null) {
            token = bundle.get(BundleUtil.TOKEN);
        }

        HttpConnection httpConnection = new HttpConnection();
        HttpURLConnection httpURLConnection = null;

        try {

            Log.i(LOG_TAG, Logger.URL + url);
            httpURLConnection = (HttpURLConnection) new URL(url).openConnection();

            if (token != null) {
                Log.i(LOG_TAG, Logger.TOKEN + String.valueOf(token));
                httpURLConnection.setRequestProperty(Header.AUTHORIZATION, TOKEN_VALUE + token);
            }

            httpURLConnection.setRequestMethod(RequestMethod.GET);
            httpURLConnection.setRequestProperty(Header.CONTENT_TYPE, PayloadType.APPLICATION_JSON);
            httpURLConnection.setRequestProperty(Header.ACCEPT_ENCODING, Constant.Symbol.NULL);
            httpURLConnection.setUseCaches(false);
            httpURLConnection.connect();

            // Connection established
            Log.i(LOG_TAG, Logger.SERVER_RESPONSE + String.valueOf(httpURLConnection.getResponseCode())
                    + Constant.Symbol.SPACE + httpURLConnection.getResponseMessage());

            httpConnection.setHttpURLConnection(httpURLConnection);
            httpConnection.setHttpConnectionSucceeded(true);
            httpConnection.setHttpConnectionCode(httpURLConnection.getResponseCode());
            httpConnection.setHttpConnectionMessage(httpURLConnection.getResponseMessage());

        } catch (Exception e) {
            e.printStackTrace();

            // Connection not established, disconnect httpURLConnection
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }

            if (!NetworkUtil.getInstance(context).isConnected()) {
                // Internet connection error
                Log.e(LOG_TAG, Logger.INTERNET_CONNECTION_ERROR);

                httpConnection.setHttpConnectionSucceeded(false);
                httpConnection.setHttpConnectionCode(HttpErrorUtil.NumericStatusCode.HTTP_NO_NETWORK);
                httpConnection.setHttpConnectionMessage(e.getMessage());

            } else if (e.getMessage().contains(HttpErrorUtil.NonNumericStatusCode.HTTP_SERVER_TIMEOUT)) {
                // Connection timed out occurred while calling server
                Log.e(LOG_TAG, Logger.CONNECTION_TIMEOUT_ERROR + url);

                httpConnection.setHttpConnectionSucceeded(false);
                httpConnection.setHttpConnectionCode(HttpErrorUtil.NumericStatusCode.HTTP_SERVER_TIMEOUT);
                httpConnection.setHttpConnectionMessage(e.getMessage());

            } else {
                // Other connection errors e.g UnknownHostException
                Log.e(LOG_TAG, e.getMessage());

                httpConnection.setHttpConnectionSucceeded(false);
                httpConnection.setHttpConnectionCode(HttpErrorUtil.NumericStatusCode.HTTP_UNKNOWN_ERROR);
                httpConnection.setHttpConnectionMessage(e.getMessage());
            }
        }

        return httpConnection;
    }

    // ===========================================================
    // Methods for/from SuperClass
    // ===========================================================

    // ===========================================================
    // Listeners, methods for/from Interfaces
    // ===========================================================

}
