package by.shapovalov.studytracker.util;

import android.widget.EditText;

import java.util.ArrayList;

public class EdittextValidateResponse {

    private ArrayList<EditText> emptyEdtList;

    public EdittextValidateResponse() {
        emptyEdtList = new ArrayList<>();
    }

    public void addErrorEdt(EditText editText) {
        emptyEdtList.add(editText);
    }

    public ArrayList<EditText> getEmptyEdtList() {
        return emptyEdtList;
    }

    public void setEmptyEdtList(ArrayList<EditText> emptyEdtList) {
        this.emptyEdtList = emptyEdtList;
    }

    public boolean isResponseSuccess() {
        return emptyEdtList.size() == 0;
    }

}
