package by.shapovalov.studytracker.util;

public class Constant {

    public class ApiURL {
        private static final String HOST = "http://192.168.72.148:8000/";

        public static final String CHAT_URL = "http://192.168.72.148:3000";

        public static final String SIGN_IN = HOST + "api/auth/signin/";
        public static final String SIGN_UP = HOST + "api/users/";
    }

    public class RequestType {
        public static final int SIGN_IN = 1;
        public static final int SIGN_UP = 2 ;
    }

    public class Code {
    }

    public class Extra {
    }

    public class Message {

        public class Extra {
            public static final String VALUE_BAD_REQUEST = "VALUE_BAD_REQUEST";
            public static final String VALUE_SIGN_IN = "VALUE_SIGN_IN";
            public static final String VALUE_SIGN_UP = "VALUE_SIGN_UP";
        }

        public class Success {
            public static final int SIGN_IN_COMPLETED = 101;
            public static final int SIGN_UP_COMPLETED = 102;
        }

        public class Error {
            public static final int NO_NETWORK = 201;
            public static final int PAGE_NOT_FOUND = 202;
            public static final int BAD_REQUEST = 203;
            public static final int UNAUTHORIZED = 204;
            public static final int UNHANDLED = 299;
        }
    }

    public class Symbol {
        public static final String ASTERISK = "*";
        public static final String NEW_LINE = "\n";
        public static final String NEW_LINES = "\n\n";
        public static final String SPACE_CHARACTER = "%20";
        public static final String SPACE = " ";
        public static final String NULL = "";
        public static final String COLON = ":";
        public static final String COMMA = ",";
        public static final String SLASH = "/";
        public static final String DOT = ".";
        public static final String UNDERLINE = "_";
        public static final String DASH = "-";
        public static final String AT = "@";
        public static final String BRACKET_OPEN = " (";
        public static final String BRACKET_CLOSE = ")";
    }

    public class Number {
    }

    public class Boolean {
    }

    public class Util {
    }
}
