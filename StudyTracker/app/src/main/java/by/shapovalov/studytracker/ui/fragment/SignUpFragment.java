package by.shapovalov.studytracker.ui.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.gson.Gson;

import by.shapovalov.studytracker.R;
import by.shapovalov.studytracker.io.rest.entity.api.SignUpRequest;
import by.shapovalov.studytracker.io.rest.service.STIntentService;
import by.shapovalov.studytracker.util.Constant;
import by.shapovalov.studytracker.util.EdittextValidateResponse;
import by.shapovalov.studytracker.util.EdittextValidator;

public class SignUpFragment extends BaseFragment implements View.OnClickListener, Handler.Callback {

    // ===========================================================
    // Constants
    // ===========================================================

    private String LOG_TAG = SignUpFragment.class.getSimpleName();

    // ===========================================================
    // Fields
    // ===========================================================

    private TextInputEditText mTieUsername;
    private TextInputEditText mTiePassword;
    private Button mBtnSignUp;

    // ===========================================================
    // Constructors
    // ===========================================================

    public static SignUpFragment newInstance() {
        return new SignUpFragment();
    }

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass
    // ===========================================================

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sign_up, container, false);
        findViews(view);
        setListeners();

        return view;
    }


    // ===========================================================
    // Listeners, methods for/from Interfaces
    // ===========================================================

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btn_sign_up:
                grabDataAndProceed(v);
                break;

        }
    }

    @Override
    public boolean handleMessage(Message msg) {
        switch (msg.what) {

            case Constant.Message.Success.SIGN_UP_COMPLETED:
                Snackbar.make(getView(), "Created User", Snackbar.LENGTH_LONG);
                break;

            //TODO: handle errors

        }

        return false;
    }

    // ===========================================================
    // Methods
    // ===========================================================

    private void setListeners() {
        mBtnSignUp.setOnClickListener(this);
    }

    private void findViews(View view) {
        mBtnSignUp = (Button) view.findViewById(R.id.btn_sign_up);
        mTieUsername = (TextInputEditText) view.findViewById(R.id.tie_sign_up_username);
        mTiePassword = (TextInputEditText) view.findViewById(R.id.tie_sign_up_password);
    }

    private void grabDataAndProceed(View clickedView) {
        EdittextValidateResponse edittextValidateResponse = EdittextValidator.checkIsEmpty(
                mTieUsername,
                mTiePassword
        );

        if (!edittextValidateResponse.isResponseSuccess()) {
            EdittextValidator.handleEmptyState(
                    edittextValidateResponse,
                    getString(R.string.edt_error)
            );

        } else {
            SignUpRequest signUpFragment = new SignUpRequest();
            signUpFragment.setEmail(mTieUsername.getText().toString());
            signUpFragment.setPassword(mTiePassword.getText().toString());
            //signUpFragment.setUsername(mTieUsername.getText().toString());

            STIntentService.start(
                    getActivity(),
                    this,
                    Constant.ApiURL.SIGN_UP,
                    Constant.RequestType.SIGN_UP,
                    new Gson().toJson(signUpFragment)
            );
        }
    }

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

}